/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/18 19:43:10 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/19 19:14:32 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parse.h"

t_token	*token_new(t_cline *token_cline)
{
	t_token	*token;

	if (!(token = (t_token *)malloc(sizeof(t_token))))
		return (NULL);
	token->cline = token_cline;
	token->next = NULL;
	return (token);
}

void	token_free(t_token *token)
{
	if (!token)
		return ;
	free_cline(token->cline);
	free(token);
}
