/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/18 19:43:12 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/21 15:56:12 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parse.h"

t_cmd	*cmd_new(void)
{
	t_cmd	*cmd;

	if (!(cmd = (t_cmd *)malloc(sizeof(t_cmd))))
		return (NULL);
	cmd->token_list = NULL;
	cmd->rd_list = NULL;
	cmd->next = NULL;
	return (cmd);
}

t_token	*cmd_push_token(t_cmd *cmd, t_token *token)
{
	t_token	*cur;

	if (!cmd->token_list)
		cmd->token_list = token;
	else
	{
		cur = cmd->token_list;
		while (cur->next)
			cur = cur->next;
		cur->next = token;
	}
	return (token);
}

void	cmd_free(t_cmd *cmd)
{
	t_token	*tmp;

	if (!cmd)
		return ;
	while (cmd->token_list)
	{
		tmp = cmd->token_list;
		cmd->token_list = cmd->token_list->next;
		token_free(tmp);
	}
	free_rd(cmd->rd_list);
	free(cmd);
}
