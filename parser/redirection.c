/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirection.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/21 15:51:12 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/21 17:14:44 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parse.h"

t_rd	*rd_new(void)
{
	t_rd	*rd;

	if (!(rd = (t_rd *)malloc(sizeof(t_rd))))
		return (NULL);
	rd->to = NULL;
	rd->from = NULL;
	rd->next = NULL;
	rd->op = OP_NOOP;
	return (rd);
}

t_rd	*rd_push(t_rd **list, t_rd *rd)
{
	t_rd	*cur;

	if (!*list)
		*list = rd;
	else
	{
		cur = *list;
		while (cur->next)
			cur = cur->next;
		cur->next = rd;
	}
	return (rd);
}

void	free_rd(t_rd *list)
{
	t_rd	*tmp;

	while (list)
	{
		tmp = list;
		list = list->next;
		free_cline(tmp->to);
		free_cline(tmp->from);
		free(tmp);
	}
}
