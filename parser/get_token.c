/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_token.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/18 17:29:08 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/19 19:14:34 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parse.h"

static bool	check_escaping(t_token_op *top)
{
	if (top->cur->ch == '\\' && top->is_qtd != '\'')
	{
		if (top->cur->next)
		{
			top->cur = top->cur->next->next;
			top->len += 2;
		}
		return (true);
	}
	else if (!top->is_qtd && (top->cur->ch == '\'' || top->cur->ch == '"'))
		top->is_qtd = (char)top->cur->ch;
	else if (top->is_qtd == top->cur->ch)
		top->is_qtd = '\0';
	return (false);
}

static bool	check_op(t_token_op *top, t_cline **src_cline,
			t_cline **token_cline)
{
	if (top->is_qtd || is_meta(top->cur->ch) == META_NOMETA)
	{
		top->len++;
		top->cur = top->cur->next;
	}
	else if (!top->cur->prev)
	{
		if ((top->op = get_op(*src_cline)) == OP_ERR)
			return (true);
		top->cur = (*src_cline)->begin;
		if (top->op == OP_CMDEND && !top->cur)
		{
			top->op = OP_END;
			return (true);
		}
	}
	else
	{
		*token_cline = *src_cline;
		*src_cline = split_cline(*token_cline, top->len);
		return (true);
	}
	return (false);
}

t_op		get_token(t_cline **src_cline, t_cline **token_cline)
{
	t_token_op	top;

	top.len = 0;
	top.is_qtd = '\0';
	if (!(top.cur = (*src_cline)->begin))
		return (OP_END);
	top.op = OP_NOOP;
	while (top.cur)
	{
		if (check_escaping(&top))
			continue ;
		if (check_op(&top, src_cline, token_cline))
			return (top.op);
	}
	*token_cline = *src_cline;
	*src_cline = new_cline();
	return (top.op);
}
