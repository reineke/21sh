/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_op.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/18 13:11:01 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/21 17:24:07 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parse.h"

static t_op	get_op2(t_cline *cline, t_meta meta_1)
{
	t_meta	meta_2;
	t_op	op;

	op = OP_ERR;
	if (!cline->begin)
		return (OP_ERR);
	meta_2 = is_meta(cline->begin->ch);
	if (meta_1 == META_LESS && (meta_2 == META_SPACE || meta_2 == META_NOMETA))
		return (OP_REIN);
	if (meta_1 == META_GREAT && (meta_2 == META_SPACE || meta_2 == META_NOMETA))
		return (OP_REOUT);
	if (meta_1 == META_LESS && meta_2 == META_LESS)
		op = OP_HDOC;
	else if (meta_1 == META_GREAT && meta_2 == META_GREAT)
		op = OP_REOUTAP;
	else if (meta_1 == META_GREAT && meta_2 == META_AMSND)
		op = OP_REOUTFD;
	else if (meta_1 == META_AMSND && meta_2 == META_GREAT)
		op = OP_REERRFD;
	if (op != OP_ERR)
		del_cch_cline(cline, cline->begin);
	return ((cline->begin) ? op : OP_ERR);
}

static t_op	get_op1(t_cline *cline)
{
	t_meta	meta_1;
	t_op	op;

	if (!cline->begin)
		return (OP_END);
	op = OP_ERR;
	meta_1 = is_meta(cline->begin->ch);
	if (meta_1 == META_NOMETA || meta_1 == META_SPACE)
		return (OP_NOOP);
	if (meta_1 == META_COLON)
		op = OP_CMDEND;
	else if (meta_1 == META_PIPE)
		op = OP_PIPE;
	if (meta_1 == META_AMSND && (!cline->begin->next ||
			is_meta(cline->begin->next->ch) != META_GREAT))
		return (OP_ERR);
	else
		del_cch_cline(cline, cline->begin);
	if (op != OP_ERR)
		return (op);
	return (get_op2(cline, meta_1));
}

static t_op	check_io(t_op op, bool io)
{
	if (op == OP_REIN && io == true)
		op = OP_REIN_IO;
	if (op == OP_REOUT && io == true)
		op = OP_REOUT_IO;
	if (op == OP_REOUTAP && io == true)
		op = OP_REOUTAP_IO;
	if (op == OP_REOUTFD && io == true)
		op = OP_REOUTFD_IO;
	return (op);
}

t_op		get_op(t_cline *cline)
{
	t_op	op;
	t_op	tmp_op;
	bool	io;

	op = OP_NOOP;
	io = true;
	while (cline->begin && is_meta(cline->begin->ch) != META_NOMETA)
	{
		while (cline->begin && is_meta(cline->begin->ch) == META_SPACE)
		{
			del_cch_cline(cline, cline->begin);
			if (op == OP_NOOP)
				io = false;
		}
		if (!cline->begin)
			return ((op == OP_NOOP || op == OP_CMDEND) ? OP_END : OP_ERR);
		if (is_meta(cline->begin->ch) == META_NOMETA)
			break ;
		if (op != OP_NOOP && is_meta(cline->begin->ch) != META_SPACE)
			return (OP_ERR);
		tmp_op = get_op1(cline);
		if (tmp_op != OP_NOOP)
			op = tmp_op;
	}
	return (check_io(op, io));
}
