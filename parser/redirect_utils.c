/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirect_utils.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/21 18:58:19 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/21 19:02:23 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parse.h"

static t_token		*get_prelast_token(t_token *token)
{
	if (!token || !token->next)
		return (NULL);
	while (token->next->next)
		token = token->next;
	return (token);
}

static inline void	to_from(t_rd *rd, t_cline *cline, t_op op)
{
	if (op == OP_REIN || op == OP_REIN_IO || op == OP_HDOC)
		rd->to = cline;
	else
		rd->from = cline;
}

void				get_redir_num(t_token **token_list, t_rd *rd, t_op op)
{
	t_token	*last_token;

	if ((*token_list)->next)
	{
		last_token = get_prelast_token(*token_list);
		if (!last_token->next->cline || !is_num_cline(last_token->next->cline))
			return ;
		to_from(rd, last_token->next->cline, op);
		free(last_token->next);
		last_token->next = NULL;
	}
	else if (*token_list && !(*token_list)->next &&
			(*token_list)->cline && is_num_cline((*token_list)->cline))
	{
		to_from(rd, (*token_list)->cline, op);
		free(*token_list);
		*token_list = NULL;
	}
}
