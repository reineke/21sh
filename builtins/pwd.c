/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pwd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 18:54:42 by mvladymy          #+#    #+#             */
/*   Updated: 2019/07/08 13:44:02 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"

extern char	g_pwd[PATH_MAX];

int			bltn_pwd(int argc, char **argv, char **env)
{
	bool	logic;
	char	pwd[PATH_MAX];

	(void)argc;
	(void)env;
	argv++;
	if (is_logic(&argv, &logic, "pwd"))
		return (1);
	if ((*argv && ft_strcmp(*argv, "-")) ||( *argv && *(argv + 1)))
	{
		ft_dprintf(STDERR_FILENO,
			"21sh: pwd: %s: yakos' zabahato vsoioho 🙄\n", *argv);
		return (1);
	}
	if (!ft_strlen(g_pwd))
		getcwd(g_pwd, PATH_MAX);
	if (logic)
		ft_strncpy(pwd, g_pwd, PATH_MAX - 1);
	else
		getcwd(pwd, PATH_MAX);
	pwd[PATH_MAX - 1] = '\0';
	ft_printf("%s\n", pwd);
	return (0);
}
