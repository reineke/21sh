/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 18:53:34 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/24 19:06:35 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtins.h"

int	bltn_echo(int argc, char **argv, char **env)
{
	int	i;

	(void)env;
	if (argc == 1)
	{
		ft_putchar('\n');
		return (0);
	}
	i = 0;
	while (i++ < argc - 1)
		ft_printf("%s%c", argv[i], (i == argc - 1) ? '\n' : ' ');
	return (0);
}
