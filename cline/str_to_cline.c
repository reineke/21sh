/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_to_cline.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/09 14:11:55 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/09 17:53:28 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

static int	str_to_wch(char *str, wchar_t *wch)
{
	if ((*str & 0xE0) == 0xC0)
	{
		*wch = ((*str << 6) & 0x7C0) + (*(str + 1) & 0x3F);
		return (2);
	}
	else if ((*str & 0xF0) == 0xE0)
	{
		*wch = ((*str << 12) & 0xF000) + ((*(str + 1) << 6) & 0xFC0)
				+ (*(str + 2) & 0x3F);
		return (3);
	}
	else if ((*str & 0xF8) == 0xF0)
	{
		*wch = ((*str << 18) & 0x1C0000) + ((*(str + 1) << 12) & 0x3F000)
				+ ((*(str + 2) << 6) & 0xFC0) + (*(str + 3) & 0x3F);
		return (4);
	}
	*wch = *str;
	return (1);
}

t_cline		*str_to_cline(char *str)
{
	t_cline	*cline;
	wchar_t	wch;

	if (!(cline = new_cline()))
		return (NULL);
	while (*str)
	{
		str += str_to_wch(str, &wch);
		push_ch(cline, wch);
	}
	return (cline);
}
