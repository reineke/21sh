/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_cline.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 17:12:52 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/05 08:36:43 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

int	print_cline(t_cch *chain)
{
	int	printed;

	printed = 0;
	while (chain)
	{
		ft_putwchar_fd(((chain->ch != '\n') ? chain->ch : ' '), STDIN_FILENO);
		printed++;
		g_scr.pos++;
		if (g_scr.pos >= g_scr.width || chain->ch == '\n')
		{
			g_scr.pos = 0;
			ft_dprintf(STDIN_FILENO, tgetstr("sf", NULL));
			ft_dprintf(STDIN_FILENO, tgetstr("cd", NULL));
		}
		chain = chain->next;
	}
	return (printed);
}
