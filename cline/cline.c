/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cline.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/19 17:17:16 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/18 13:13:37 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

t_cline	*new_cline(void)
{
	t_cline	*cline;

	cline = (t_cline*)malloc(sizeof(t_cline));
	if (!cline)
		return (NULL);
	cline->begin = NULL;
	cline->end = NULL;
	cline->cursor = NULL;
	cline->len = 0;
	cline->pos = 0;
	return (cline);
}

void	clear_cline(t_cline *cline)
{
	t_cch	*cur_chain;
	t_cch	*tmp;

	cur_chain = cline->begin;
	while (cur_chain)
	{
		tmp = cur_chain;
		cur_chain = cur_chain->next;
		del_cch(tmp);
	}
	cline->begin = NULL;
	cline->end = NULL;
	cline->cursor = NULL;
	cline->len = 0;
	cline->pos = 0;
}

void	free_cline(t_cline *cline)
{
	if (!cline)
		return ;
	clear_cline(cline);
	free(cline);
}

t_cline	*copy_cline(t_cline *old, int len)
{
	t_cline	*new;
	t_cch	*cur_ch;

	new = new_cline();
	if (!new)
		return (NULL);
	len = (len) ? len : old->len;
	cur_ch = old->begin;
	while (cur_ch && len--)
	{
		if (!push_ch(new, cur_ch->ch))
		{
			free_cline(new);
			return (NULL);
		}
		cur_ch = cur_ch->next;
	}
	return (new);
}

void	del_cch_cline(t_cline *cline, t_cch *cch)
{
	if (cch == cline->begin)
		cline->begin = cch->next;
	if (cch == cline->end)
		cline->end = cch->prev;
	if (cch == cline->cursor)
		cline->cursor = cch->next;
	del_cch(cch);
	cline->len--;
	cline->pos--;
}
