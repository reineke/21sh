/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history_stack_move.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/07 18:07:17 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/11 16:28:11 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

t_hist			*g_hist;
t_hist			*g_hist_cur;

static t_cline	*g_last_cline;

void			next_hist_node(t_cline **cline_ptr)
{
	if (!g_hist_cur)
		return ;
	free_cline(g_hist_cur->cline);
	g_hist_cur->cline = *cline_ptr;
	g_hist_cur = g_hist_cur->next;
	if (g_hist_cur)
		*cline_ptr = copy_cline(g_hist_cur->cline, 0);
	else if (g_last_cline)
		*cline_ptr = copy_cline(g_last_cline, 0);
}

void			prev_hist_node(t_cline **cline_ptr)
{
	if ((g_hist_cur && !g_hist_cur->prev) || !g_hist)
		return ;
	if (!g_hist_cur)
	{
		g_hist_cur = g_hist;
		free_cline(g_last_cline);
		g_last_cline = *cline_ptr;
		*cline_ptr = copy_cline(g_hist_cur->cline, 0);
	}
	else
	{
		free_cline(g_hist_cur->cline);
		g_hist_cur->cline = *cline_ptr;
		g_hist_cur = g_hist_cur->prev;
		*cline_ptr = copy_cline(g_hist_cur->cline, 0);
	}
}

void			reset_hist_cursor(void)
{
	free_cline(g_last_cline);
	g_last_cline = NULL;
	g_hist_cur = NULL;
}
