/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_cursor.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 17:13:07 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/03 17:10:05 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

static void	move_cursor_right(int to_move)
{
	while (to_move)
	{
		if (g_scr.pos >= g_scr.width - 1)
		{
			g_scr.pos = 0;
			ft_dprintf(STDIN_FILENO, tgetstr("do", NULL));
			to_move--;
		}
		else
		{
			g_scr.pos++;
			ft_dprintf(STDIN_FILENO, tgetstr("nd", NULL));
			to_move--;
		}
	}
}

static void	move_cursor_left(int to_move)
{
	int	i;

	while (to_move)
	{
		if (g_scr.pos <= 0)
		{
			g_scr.pos = g_scr.width - 1;
			ft_dprintf(STDIN_FILENO, tgetstr("up", NULL));
			i = g_scr.width - 1;
			while (i--)
				ft_dprintf(STDIN_FILENO, tgetstr("nd", NULL));
			to_move--;
		}
		else
		{
			g_scr.pos--;
			ft_dprintf(STDIN_FILENO, tgetstr("le", NULL));
			to_move--;
		}
	}
}

void		move_cursor(int to_move)
{
	if (to_move > 0)
		move_cursor_right(to_move);
	else
		move_cursor_left(-to_move);
}
