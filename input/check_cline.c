/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_cline.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/02 18:43:15 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/02 18:52:12 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

static inline t_cause	check_value(t_cheker *checks)
{
	if (checks->quote)
		return (QUOTE);
	else if (checks->dquote)
		return (DQUOTE);
	else if (checks->pipe)
		return (PIPE);
	else if (checks->bslsh % 2)
		return (SLSH);
	else
		return (OK);
}

static inline void		init_checker(t_cheker *checker)
{
	checker->quote = false;
	checker->dquote = false;
	checker->pipe = false;
	checker->bslsh = 0;
}

t_cause					check_cline(t_cline *cline)
{
	t_cch		*cch;
	t_cheker	checks;

	init_checker(&checks);
	cch = cline->begin;
	while (cch)
	{
		if (!ft_isspace((char)cch->ch))
			checks.pipe = false;
		if (cch->ch == '"' && !(checks.bslsh % 2))
			checks.dquote = (checks.quote) ? checks.dquote : !checks.dquote;
		else if (cch->ch == '\'' && !(checks.bslsh % 2))
			checks.quote = (checks.dquote) ? checks.quote : !checks.quote;
		else if (cch->ch == '|' && !(checks.bslsh % 2))
			checks.pipe = true;
		else if (cch->ch == '\\' && !checks.quote)
			checks.bslsh++;
		else
			checks.bslsh = 0;
		cch = cch->next;
	}
	return (check_value(&checks));
}
