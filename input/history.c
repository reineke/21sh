/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/31 17:15:18 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/07 18:09:21 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

extern t_hist	*g_hist;

t_hist	*new_hist_node(t_cline *cline)
{
	t_hist	*new_node;

	new_node = (t_hist *)malloc(sizeof(t_hist));
	if (!new_node)
		return (NULL);
	new_node->prev = NULL;
	new_node->next = NULL;
	new_node->cline = cline;
	return (new_node);
}

void	push_hist_node(t_hist *node)
{
	if (!node)
		return ;
	if (!g_hist)
		g_hist = node;
	else
	{
		g_hist->next = node;
		node->prev = g_hist;
		g_hist = node;
	}
	reset_hist_cursor();
}

void	free_hist(t_hist_free_hdlr cline_hdlr)
{
	t_hist	*tmp;

	while (g_hist)
	{
		if (cline_hdlr)
			cline_hdlr(g_hist->cline);
		tmp = g_hist;
		free_cline(tmp->cline);
		free(tmp);
		g_hist = g_hist->prev;
	}
	reset_hist_cursor();
}
