/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/19 17:17:54 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/28 12:35:48 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

static struct termios	g_term;

void					set_term(void)
{
	struct termios	term;
	char			*term_name;

	tcgetattr(0, &term);
	if (ft_memcmp(&term, &g_term, sizeof(struct termios)))
		g_term = term;
	term.c_lflag &= (unsigned int)~(ICANON | ECHO | ECHONL);
	tcsetattr(0, TCSANOW, &term);
	term_name = getenv("TERM");
	if (!term_name || !ft_strlen(term_name))
		term_name = "xterm";
	tgetent(NULL, term_name);
	g_scr.width = tgetnum("co");
}

void					unset_term(void)
{
	tcsetattr(0, TCSANOW, &g_term);
}
