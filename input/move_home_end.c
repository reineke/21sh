/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_home_end.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 17:47:14 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/03 20:06:58 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

void	home(t_cline **cline_ptr)
{
	move_left_for(cline_ptr, (*cline_ptr)->pos);
}

void	end(t_cline **cline_ptr)
{
	move_right_for(cline_ptr, (*cline_ptr)->len - (*cline_ptr)->pos);
}
