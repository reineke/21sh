/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/01 18:36:32 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/29 18:34:24 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

int	g_prompt_len;

int	prompt_1(void)
{
	ft_dprintf(STDIN_FILENO, tgetstr("sc", NULL));
	g_prompt_len =
		ft_dprintf(STDERR_FILENO, "%c> ", (getuid()) ? '$' : '#');
	g_scr.pos = g_prompt_len % g_scr.width;
	return (g_prompt_len);
}

int	prompt_2(t_cause cause)
{
	const char	*s[6] = {
		"",
		"qt",
		"dqt",
		"pipe",
		"",
		"hdoc"
	};

	ft_dprintf(STDIN_FILENO, tgetstr("sc", NULL));
	g_prompt_len = ft_dprintf(STDERR_FILENO, "%s> ", s[cause]);
	g_scr.pos = g_prompt_len % g_scr.width;
	return (g_prompt_len);
}

int	prompt(t_cause cause)
{
	static t_cause	old_cause;

	if (cause != NO_CAUSE)
		old_cause = cause;
	if (old_cause == OK)
		return (prompt_1());
	else
		return (prompt_2(old_cause));
}
