/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   esc_seq.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 18:29:12 by mvladymy          #+#    #+#             */
/*   Updated: 2019/04/29 19:54:23 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

static t_esc_cmp	seq_cmp(const t_esc_seq esc_seq, const t_esc_seq cur_seq)
{
	unsigned int	i;

	i = 0;
	while (cur_seq[i] == esc_seq[i] && esc_seq[i] != 0 && i < ESC_BUF_SIZE)
		i++;
	if (i == ESC_BUF_SIZE || (esc_seq[i] == 0 && cur_seq[i] == 0))
		return (ESC_CMP_EQ);
	else if (cur_seq[i] == 0)
		return (ESC_CMP_CON);
	else
		return (ESC_CMP_NEQ);
}

t_seq_num			get_esc_seq(void)
{
	t_esc_seq		cur_seq;
	unsigned int	pos;
	t_seq_num		seq_num;
	t_esc_cmp		tmp_resp;
	t_esc_cmp		resp;

	ft_bzero(cur_seq, ESC_BUF_SIZE);
	pos = 0;
	while (pos < ESC_BUF_SIZE)
	{
		resp = ESC_CMP_NEQ;
		read(STDIN_FILENO, &cur_seq[pos++], 1);
		seq_num = 0;
		while (seq_num < g_codes_count)
		{
			if ((tmp_resp =
					seq_cmp(g_esc_codes[seq_num++], cur_seq)) == ESC_CMP_EQ)
				return (seq_num);
			else if (tmp_resp == ESC_CMP_CON)
				resp = tmp_resp;
		}
		if (resp == ESC_CMP_NEQ)
			break ;
	}
	return (ESC_FAIL);
}
