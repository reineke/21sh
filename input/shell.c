/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/05 08:30:06 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/09 19:26:13 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "input.h"

static t_cline	*cat_cline(t_cline *c1, t_cline *c2)
{
	if (!c1 || !c2)
	{
		free_cline(c1);
		free_cline(c2);
		ft_putchar_fd('\n', STDIN_FILENO);
		return (NULL);
	}
	c1->cursor = NULL;
	if (c1->end && c1->end->ch == '\\')
		c1->end->ch = '\n';
	else
		push_ch(c1, '\n');
	if (c1->end)
		c1->end->next = c2->begin;
	if (c2->begin)
		c2->begin->prev = c1->end;
	if (c2->end)
		c1->end = c2->end;
	c1->len += c2->len;
	c1->pos = c1->len;
	free(c2);
	return (c1);
}

static t_rs		empty_cline(t_cline *cline)
{
	end(&cline);
	ft_putchar_fd('\n', STDIN_FILENO);
	free_cline(cline);
	reset_hist_cursor();
	return (READ_DROP);
}

static t_rs		read_ext_cline(t_cline *cline, t_cause check)
{
	t_cline	*ext_cline;
	t_rs	rs;

	end(&cline);
	ft_putchar_fd('\n', STDIN_FILENO);
	prompt(check);
	rs = read_cline(&ext_cline);
	if (rs != READ_OK)
	{
		end(&cline);
		ft_putchar_fd('\n', STDIN_FILENO);
		reset_hist_cursor();
		free_cline(cline);
		free_cline(ext_cline);
		return (READ_DROP);
	}
	if (!cat_cline(cline, ext_cline))
		return (READ_DROP);
	return (READ_OK);
}

static t_rs		run_shell(t_cline_hdlr hdlr)
{
	t_hist	*node;
	t_cline	*cline;
	t_cause	check;
	t_rs	rs;

	prompt(OK);
	if (read_cline(&cline) == READ_EXIT)
		return (READ_EXIT);
	while ((check = check_cline(cline)))
		if ((rs = read_ext_cline(cline, check)) != READ_OK)
			return (rs);
	if (is_space_cline(cline))
		return (empty_cline(cline));
	if (!(node = new_hist_node(cline)))
	{
		free_cline(cline);
		return (READ_EXIT);
	}
	push_hist_node(node);
	end(&cline);
	ft_putchar_fd('\n', STDIN_FILENO);
	if (hdlr)
		return (hdlr(cline));
	return (READ_EXIT);
}

void			shell_loop(t_cline_hdlr hdlr)
{
	set_sign();
	set_term();
	while (run_shell(hdlr) != READ_EXIT)
		;
	ft_putstr_fd("exit\n", STDERR_FILENO);
	free_hist(NULL);
	unset_term();
	unset_sign();
}
