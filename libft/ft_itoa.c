/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 18:30:52 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/09 11:49:44 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	num_len(long int n)
{
	size_t	len;

	len = 1;
	if (n < 0)
	{
		n = -n;
		len++;
	}
	while (n >= 10)
	{
		n /= 10;
		len++;
	}
	return (len);
}

char			*ft_itoa(int n)
{
	size_t			len;
	unsigned int	t_n;
	char			*str_num;

	len = num_len(n);
	if (!(str_num = ft_strnew(len)))
		return (NULL);
	t_n = (unsigned int)n;
	if (n < 0)
	{
		str_num[0] = '-';
		t_n = (unsigned int)-n;
	}
	if (!t_n)
		str_num[--len] = '0';
	while (t_n)
	{
		str_num[--len] = (char)((t_n % 10) + '0');
		t_n /= 10;
	}
	return (str_num);
}
