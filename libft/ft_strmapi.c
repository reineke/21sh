/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 10:14:39 by mvladymy          #+#    #+#             */
/*   Updated: 2017/10/26 17:18:18 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(const char *s, char (*f)(unsigned int, char))
{
	size_t			len;
	char			*map;

	if (!s || !f)
		return (NULL);
	len = ft_strlen(s);
	map = ft_strnew(len);
	if (!map)
		return (NULL);
	while (len--)
		map[len] = f((unsigned int)len, s[len]);
	return (map);
}
