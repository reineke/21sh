/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_signed_num.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 16:06:33 by mvladymy          #+#    #+#             */
/*   Updated: 2018/02/17 16:06:59 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>

void			read_signed_num(long long int num, t_conv *atr, uint8_t base)
{
	size_t	unsigned_num;

	unsigned_num = (num < 0) ? -num : num;
	read_num(unsigned_num, atr, base, num < 0);
}
