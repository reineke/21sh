/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 14:42:29 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/07 15:45:43 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	unsigned int	num;
	unsigned int	digits;
	unsigned int	temp;

	if (n < 0)
		ft_putchar_fd('-', fd);
	num = (n > 0) ? (unsigned int)n : (unsigned int)-n;
	digits = 1;
	temp = num;
	while (temp >= 10)
	{
		digits *= 10;
		temp /= 10;
	}
	while (digits)
	{
		ft_putchar_fd((char)((num / digits) + '0'), fd);
		num %= digits;
		digits /= 10;
	}
}
