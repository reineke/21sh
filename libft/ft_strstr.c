/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 18:53:12 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/09 14:59:45 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	int		idx_h;
	int		idx_n;

	if (!ft_strcmp(needle, ""))
		return ((char *)haystack);
	idx_h = 0;
	while (haystack[idx_h] != '\0')
	{
		idx_n = 0;
		while (haystack[idx_h + idx_n] == needle[idx_n]
				&& needle[idx_n] != '\0')
			idx_n++;
		if (needle[idx_n] == '\0')
			return ((char *)&haystack[idx_h]);
		idx_h++;
	}
	return (NULL);
}
