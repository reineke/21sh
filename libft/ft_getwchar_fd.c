/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getwchar_fd.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 16:35:28 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/07 20:10:57 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

static inline wchar_t	r16bit(char *buf, int fd)
{
	read(fd, &buf[1], 1);
	return (((buf[0] << 6) & 0x7C0) + (buf[1] & 0x3F));
}

static inline wchar_t	r24bit(char *buf, int fd)
{
	read(fd, &buf[1], 2);
	return (((buf[0] << 12) & 0xF000)
			+ ((buf[1] << 6) & 0xFC0)
			+ (buf[2] & 0x3F));
}

static inline wchar_t	r32bit(char *buf, int fd)
{
	read(fd, &buf[1], 3);
	return (((buf[0] << 18) & 0x1C0000)
			+ ((buf[1] << 12) & 0x3F000)
			+ ((buf[2] << 6) & 0xFC0)
			+ (buf[3] & 0x3F));
}

wchar_t					ft_getwchar_fd(int fd)
{
	char	buf[4];
	wchar_t	ch;

	if (!~read(fd, &buf[0], 1))
		return (-1);
	if ((buf[0] & 0xE0) == 0xC0)
		ch = r16bit(buf, fd);
	else if ((buf[0] & 0xF0) == 0xE0)
		ch = r24bit(buf, fd);
	else if ((buf[0] & 0xF8) == 0xF0)
		ch = r32bit(buf, fd);
	else
		ch = buf[0];
	return (ch);
}
