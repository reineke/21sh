/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 19:41:44 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/09 14:59:34 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t	idx_h;
	size_t	idx_n;

	if (!ft_strcmp(needle, ""))
		return ((char *)haystack);
	idx_h = 0;
	while (haystack[idx_h] != '\0')
	{
		idx_n = 0;
		while (haystack[idx_h + idx_n] == needle[idx_n]
				&& needle[idx_n] != '\0')
			idx_n++;
		if (needle[idx_n] == '\0' && (idx_h + idx_n <= len))
			return ((char *)&haystack[idx_h]);
		idx_h++;
	}
	return (NULL);
}
