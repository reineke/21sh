/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_buff.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 14:29:20 by mvladymy          #+#    #+#             */
/*   Updated: 2018/03/12 14:47:13 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	num_len(long int n)
{
	size_t	len;

	len = 1;
	if (n < 0)
	{
		n = -n;
		len++;
	}
	while (n >= 10)
	{
		n /= 10;
		len++;
	}
	return (len);
}

char			*ft_itoa_buff(int n, char *buff, size_t buff_size)
{
	size_t			len;
	unsigned int	t_n;

	len = num_len(n);
	if (len > buff_size)
		return (NULL);
	t_n = (unsigned int)n;
	if (n < 0)
	{
		buff[0] = '-';
		t_n = (unsigned int)-n;
	}
	buff[len] = '\0';
	if (!t_n)
		buff[--len] = '0';
	while (t_n)
	{
		buff[--len] = (char)((t_n % 10) + '0');
		t_n /= 10;
	}
	return (buff);
}
