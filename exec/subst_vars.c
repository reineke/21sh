/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   subst_vars.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 17:31:07 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/25 13:55:48 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

void	subst_home_var(t_cline *cline, t_cch **cch)
{
	t_cline	*home_cline;
	char	*home_str;

	if (!(home_str = get_env_var(g_env, "HOME")))
		return ;
	del_cch_cline(cline, cline->begin);
	while (*home_str && *home_str != '=' && !ft_isspace(*home_str))
		home_str++;
	if (*home_str)
		home_str++;
	if (!(home_cline = str_to_cline(home_str)))
		return ;
	insert_value(cline, cline->begin, home_cline);
	*cch = cline->begin;
}

void	subst_vars(t_cline *cline)
{
	t_cch	*cch;
	char	is_qtd;

	is_qtd = '\0';
	if (!(cch = cline->begin))
		return ;
	if (cch->ch == '~' && (!cch->next || cch->next->ch == '/'))
		subst_home_var(cline, &cch);
	while (cch)
	{
		if ((cch->ch == '\'' || cch->ch == '"') &&
				(is_qtd == cch->ch || is_qtd == '\0'))
			is_qtd = (is_qtd) ? '\0' : (char)cch->ch;
		if (is_qtd != '\'' && cch->ch == '\\' && cch->next)
			cch = cch->next->next;
		else if (is_qtd != '\'' && cch->ch == '$' && cch->next &&
				!is_var_end((cch->next->ch)))
			subst_var(cline, &cch);
		else
			cch = cch->next;
	}
}
