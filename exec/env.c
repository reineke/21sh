/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 13:55:00 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/26 16:11:56 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

void	free_env(char *env[])
{
	int	ctr;

	ctr = 0;
	while (env[ctr])
		free(env[ctr++]);
	free(env);
}

bool	check_env_str(char *env_str, char *var)
{
	int	ctr;

	ctr = 0;
	while (env_str[ctr] != '\0' && env_str[ctr] != '=' && var[ctr] != '\0')
	{
		if (env_str[ctr] != var[ctr])
			return (false);
		ctr++;
	}
	return (env_str[ctr] == '=' && var[ctr] == '\0');
}

char	*get_env_var(char *env[], char *var)
{
	int	ctr;

	ctr = 0;
	while (env[ctr])
	{
		if (check_env_str(env[ctr], var))
			return (env[ctr]);
		ctr++;
	}
	return (NULL);
}

char	*get_var_value(char *var)
{
	if (!var)
		return (NULL);
	while (*var && *var != '=')
		var++;
	return ((*var == '=') ? var + 1 : var);
}

char	*set_env_var(char **env[], char *var, char *value)
{
	char			*var_str;
	char			*result_var;

	if (!(var_str = ft_strjoin(var, "=")))
		return (NULL);
	ft_stradd(&var_str, value);
	result_var = push_env_var(env, var_str);
	free(var_str);
	return (result_var);
}
