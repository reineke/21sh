/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   argv.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 17:25:36 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/27 15:51:15 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

static void	del_empty_token(t_token **begin, t_token *prev, t_token **cur)
{
	t_token	*tmp;

	tmp = *cur;
	if (!prev)
		*begin = (*cur)->next;
	else
		prev->next = (*cur)->next;
	*cur = (*cur)->next;
	token_free(tmp);
}

static int	prepare_list(t_token **list)
{
	t_token	*list_cur;
	t_token	*list_prev;
	int		word_cnt;

	word_cnt = 0;
	list_cur = *list;
	list_prev = NULL;
	while (list_cur)
	{
		subst_vars(list_cur->cline);
		if (!list_cur->cline->begin)
			del_empty_token(list, list_prev, &list_cur);
		else
		{
			del_meta_char(list_cur->cline);
			word_cnt++;
			list_prev = list_cur;
			list_cur = list_cur->next;
		}
	}
	return (word_cnt);
}

int			get_argv(t_token **token_list, char **argv_ptr[])
{
	t_token	*cur;
	int		word_cnt;
	int		i;

	word_cnt = prepare_list(token_list);
	if (!(*argv_ptr = (char **)malloc(sizeof(char *)
			* (unsigned int)(word_cnt + 1))))
		return (-1);
	cur = *token_list;
	i = 0;
	(*argv_ptr)[word_cnt] = NULL;
	while (cur)
	{
		if (!((*argv_ptr)[i++] = cline_to_str(cur->cline)))
			return (i);
		cur = cur->next;
	}
	return (i);
}

void		free_argv(int argc, char **argv)
{
	while (argc--)
		free(argv[argc]);
	free(argv);
}
