/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   start_env.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/26 16:13:31 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/26 16:20:33 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

void	start_env(char **env[])
{
	char	path[PATH_MAX];
	char	*sh_lvl;
	char	sh_lvl_num[32];

	getcwd(path, PATH_MAX);
	set_env_var(env, "PWD", path);
	if (!(sh_lvl = get_var_value(get_env_var(*env, "SHLVL"))))
		set_env_var(env, "SHLVL", "1");
	else
	{
		ft_itoa_buff(ft_atoi(sh_lvl) + 1, sh_lvl_num, 32);
		set_env_var(env, "SHLVL", sh_lvl_num);
	}
}
