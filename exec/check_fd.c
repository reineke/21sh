/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_fd.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 20:26:58 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/28 20:27:35 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

int	check_fd(t_cline *fd_cline)
{
	int	fd;

	if (!fd_cline)
		return (-1);
	if (!is_num_cline(fd_cline))
		return (-1);
	fd = cline_to_int(fd_cline);
	if (!STDFD(fd))
		return (-1);
	return (fd);
}
