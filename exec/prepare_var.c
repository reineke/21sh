/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prepare_var.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 17:43:56 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/24 18:14:48 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

bool	is_var_end(wchar_t c)
{
	return (!ft_isalnum(c) && c != '_' && c < 128);
}

t_cch	*del_var(t_cline *cline, t_cch *cch)
{
	t_cch	*tmp;

	if (cch->ch != '$' || !cch->next || is_var_end((char)cch->next->ch))
		return (cch);
	tmp = cch;
	cch = cch->next;
	del_cch_cline(cline, tmp);
	while (cch && !is_var_end(cch->ch))
	{
		tmp = cch;
		cch = cch->next;
		del_cch_cline(cline, tmp);
	}
	return (cch);
}

char	*get_var_name(t_cch *cch)
{
	t_cline	*var_cline;
	char	*var;

	if (cch->ch != '$' || !cch->next || is_var_end((char)cch->next->ch))
		return (NULL);
	cch = cch->next;
	if (!(var_cline = new_cline()))
		return (NULL);
	while (cch && !is_var_end(cch->ch))
	{
		if (!push_ch(var_cline, cch->ch))
		{
			free_cline(var_cline);
			return (NULL);
		}
		cch = cch->next;
	}
	var = cline_to_str(var_cline);
	free_cline(var_cline);
	return (var);
}
