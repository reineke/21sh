/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   replace_env_var.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/26 13:55:21 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/26 16:01:42 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

static char	*replace(char *src, char **dst)
{
	char	*tmp;

	tmp = *dst;
	*dst = src;
	free(tmp);
	return (*dst);
}

char		*replace_env_var_str(char *env[], char *var_str)
{
	char			*old_var;
	char			*new_var;
	unsigned int	ctr;
	char			*delim;

	if (!(delim = ft_strchr(var_str, '=')))
		return (NULL);
	*delim = '\0';
	if (!(old_var = get_env_var(env, var_str)))
		return (NULL);
	*delim = '=';
	if (!(new_var = ft_strdup(var_str)))
		return (NULL);
	ctr = 0;
	while (env[ctr])
	{
		if (env[ctr] == old_var)
			return (replace(new_var, &env[ctr]));
		ctr++;
	}
	free(new_var);
	return (NULL);
}

char		*replace_env_var(char *env[], char *var, char *value)
{
	char			*var_str;
	char			*result_str;

	if (!get_env_var(env, var))
		return (NULL);
	if (!(var_str = ft_strjoin(var, "=")))
		return (NULL);
	ft_stradd(&var_str, value);
	result_str = replace_env_var_str(env, var_str);
	free(var_str);
	return (result_str);
}
