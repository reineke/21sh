/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   subst_var.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 17:26:44 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/24 18:29:02 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

void	subst_var(t_cline *cline, t_cch **cch)
{
	t_cline	*var_cline;
	char	*var;
	char	*value;

	if (!(var = get_var_name(*cch)))
		return ;
	*cch = del_var(cline, *cch);
	value = get_env_var(g_env, var);
	free(var);
	if (!value)
		return ;
	value = get_var_value(value);
	if (*value == '=')
		value++;
	if (!(var_cline = str_to_cline(value)))
		return ;
	insert_value(cline, *cch, var_cline);
}
