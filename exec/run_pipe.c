/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   run_pipe.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/24 17:48:49 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/29 20:17:38 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

static void			print_exec_error(int signal)
{
	if (signal == SIGSEGV)
		ft_dprintf(STDERR_FILENO, "21sh: SEGFAULT. ce fiasko, bratan! 🙈\n");
	else if (signal == SIGINT)
		ft_dprintf(STDERR_FILENO, "\n");
	else if (signal == SIGTERM)
		ft_dprintf(STDERR_FILENO, "21sh: SIGTERM. ce kinec 👻\n");
	else if (signal == SIGBUS)
		ft_dprintf(STDERR_FILENO, "21sh: SIGBUS. wroom-wroom, kurwo 🚛\n");
	else if (signal == SIGABRT)
		ft_dprintf(STDERR_FILENO, "21sh: SIGABOOOORT in da house!!!1📛\n");
}

static inline void	init_fd(t_fd *fd)
{
	fd->stdin = STDIN_FILENO;
	fd->stdout = STDOUT_FILENO;
	fd->stderr = STDERR_FILENO;
	fd->pipe[0] = -1;
	fd->pipe[1] = -1;
	fd->hdoc[0] = -1;
	fd->hdoc[1] = -1;
	fd->hdoc_cline = NULL;
}

static bool			create_pipe(t_cmd *cmd, t_fd *cmd_fd)
{
	int	exit_status;

	if (cmd->next)
	{
		if (pipe(cmd_fd->pipe) < 0)
			return (-1);
		cmd_fd->stdout = cmd_fd->pipe[1];
	}
	else
		cmd_fd->stdout = STDOUT_FILENO;
	if (redirect(cmd_fd, cmd->rd_list))
		return (-1);
	exit_status = run_cmd(cmd, cmd_fd);
	cmd_fd->stdin = cmd_fd->pipe[0];
	return (exit_status);
}

int					run_pipe(t_pipe *pipeline)
{
	t_cmd	*cmd_cur;
	int		exit_status;
	t_fd	cmd_fd;

	init_fd(&cmd_fd);
	exit_status = 0;
	cmd_cur = pipeline->cmd_list;
	unset_term();
	while (cmd_cur)
	{
		exit_status = create_pipe(cmd_cur, &cmd_fd);
		cmd_cur = cmd_cur->next;
	}
	if (cmd_fd.stderr != STDERR_FILENO)
		close(cmd_fd.stderr);
	while (wait(&exit_status) != -1 || errno != ECHILD)
		;
	if (exit_status)
		print_exec_error(exit_status);
	set_term();
	return (exit_status);
}

int					run_pipe_list(t_pipe *pipe)
{
	int	exit_status;

	exit_status = 0;
	while (pipe)
	{
		exit_status = run_pipe(pipe);
		pipe = pipe->next;
	}
	return (exit_status);
}
