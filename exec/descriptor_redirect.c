/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   descriptor_redirect.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 19:56:41 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/30 15:11:12 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

static int	descriptor_error(t_fd *fd, char *err_msg)
{
	ft_dprintf(STDERR_FILENO, "21sh: %s\n", err_msg);
	if (fd->stdout != STDOUT_FILENO)
		close(fd->stdout);
	return (-1);
}

static int	stdout_redirect(t_fd *fd, int dest_fd)
{
	if (dest_fd == STDIN_FILENO)
		dup2(fd->stdin, fd->stdout);
	else if (dest_fd == STDERR_FILENO)
		dup2(fd->stderr, fd->stdout);
	return (0);
}

static int	stderr_redirect(t_fd *fd, int dest_fd)
{
	if (dest_fd == STDOUT_FILENO)
		dup2(fd->stdout, fd->stderr);
	else if (dest_fd == STDIN_FILENO)
		dup2(fd->stdin, fd->stderr);
	return (0);
}

static int	stdin_redirect(t_fd *fd, int dest_fd)
{
	if (dest_fd == STDOUT_FILENO)
		dup2(fd->stdout, fd->stdin);
	else if (dest_fd == STDERR_FILENO)
		dup2(fd->stderr, fd->stdin);
	return (0);
}

int			descriptor_redirect(t_rd *rd, t_fd *fd)
{
	int	red_fd;
	int	dest_fd;

	dest_fd = check_fd(rd->to);
	red_fd = check_fd(rd->from);
	if (rd->to && is_num_cline(rd->to) && !STDFD(dest_fd))
		return (descriptor_error(fd, "normalniy potik obery 😬"));
	else if (rd->to && !is_num_cline(rd->to))
		return (descriptor_error(fd, "pyshy do potoku 😶"));
	else if (!rd->from || rd->op == OP_REOUTFD || red_fd == STDOUT_FILENO)
		stdout_redirect(fd, dest_fd);
	else if (red_fd == STDERR_FILENO)
		stderr_redirect(fd, dest_fd);
	else if (red_fd == STDIN_FILENO)
		stdin_redirect(fd, dest_fd);
	return (0);
}
