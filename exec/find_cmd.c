/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_cmd.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/27 13:19:24 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/27 17:01:19 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

static t_path	check_path(char *path)
{
	struct stat	file_stat;

	stat(path, &file_stat);
	if (access(path, F_OK))
		return (PATH_NOEXIST);
	else if (S_ISDIR(file_stat.st_mode))
		return (PATH_ISDIR);
	else if (access(path, X_OK))
		return (PATH_NOPERM);
	return (PATH_OK);
}

static void		make_path(char *path, const char **path_var, char *cmd_name)
{
	const char	*path_end;
	size_t		path_len;

	path_end = ft_strchr(*path_var, ':');
	if (path_end == *path_var)
		(*path_var)++;
	if (path_end)
		path_end++;
	path_len = (path_end) ?
			(size_t)(path_end - *path_var) : ft_strlen(*path_var) + 1;
	path[0] = '\0';
	ft_strlcat(path, *path_var, MIN(PATH_MAX, path_len));
	if (path_len > 1 && path[path_len - 2] != '/')
		ft_strlcat(path, "/", PATH_MAX);
	ft_strlcat(path, cmd_name, PATH_MAX);
	*path_var = path_end;
}

t_path			find_cmd(char *cmd_name, const char *path_var, char *cmd_path)
{
	t_path	last_check;

	if (ft_strchr(cmd_name, '/'))
	{
		cmd_path[0] = '\0';
		ft_strlcat(cmd_path, cmd_name, PATH_MAX);
		return (check_path(cmd_name));
	}
	while (path_var)
	{
		make_path(cmd_path, &path_var, cmd_name);
		if ((last_check = check_path(cmd_path)) == PATH_OK)
			break ;
	}
	return (last_check);
}
