/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_redirect.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 18:29:44 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/29 18:50:27 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

static int			open_file(t_cline *file_cline, int mode)
{
	char	*file;
	int		fd;

	if (!file_cline)
		return (-1);
	if (!(file = cline_to_str(file_cline)))
		return (-1);
	if (!~(fd = open(file, mode, FILE_RIGHTS)))
	{
		if (errno == EACCES)
			ft_dprintf(STDERR_FILENO,
				"21sh: %s: vono ne vidkryvayetsia ☹️i\n", file);
		else if (errno == ENOENT && mode == READ_MODE)
			ft_dprintf(STDERR_FILENO, "21sh: %s: nema takogo failu 😨\n", file);
	}
	free(file);
	return (fd);
}

static inline void	change_fd(int *dst, int src, int cmp)
{
	if (*dst != cmp)
		close(*dst);
	*dst = src;
}

int					write_file_redirect(t_rd *rd, t_fd *fd)
{
	int	red_fd;
	int	dest_fd;

	if (!~(dest_fd = open_file(rd->to,
			((rd->op == OP_REOUTAP) ? APPND_MODE : TRUNC_MODE))))
		return (-1);
	red_fd = check_fd(rd->from);
	if (!rd->from || rd->op == OP_REOUT || rd->op == OP_REOUTAP
			|| red_fd == STDOUT_FILENO)
		change_fd(&fd->stdout, dest_fd, STDOUT_FILENO);
	else if (red_fd == STDIN_FILENO)
		change_fd(&fd->stdin, dest_fd, STDIN_FILENO);
	else if (red_fd == STDERR_FILENO)
		change_fd(&fd->stderr, dest_fd, STDERR_FILENO);
	else
		close(dest_fd);
	return (0);
}

int					read_file_redirect(t_rd *rd, t_fd *fd)
{
	int	red_fd;
	int	dest_fd;

	if (!~(dest_fd = open_file(rd->from, READ_MODE)))
		return (-1);
	red_fd = check_fd(rd->to);
	if (!rd->to || rd->op == OP_REIN || red_fd == STDIN_FILENO)
		change_fd(&fd->stdin, dest_fd, STDIN_FILENO);
	else if (red_fd == STDOUT_FILENO)
		change_fd(&fd->stdout, dest_fd, STDOUT_FILENO);
	else if (red_fd == STDERR_FILENO)
		change_fd(&fd->stderr, dest_fd, STDERR_FILENO);
	else
		close(dest_fd);
	return (0);
}
