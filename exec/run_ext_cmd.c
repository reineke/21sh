/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   run_ext_cmd.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/26 17:48:05 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/30 15:08:47 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

static int			print_cmd_error(t_path status, char *path)
{
	if (status == PATH_NOEXIST)
		ft_dprintf(STDERR_FILENO, "21sh: %s: nema takogo failu 😐\n", path);
	else if (status == PATH_ISDIR)
		ft_dprintf(STDERR_FILENO,
				"21sh: %s: ce directoriya 🙄\n", path);
	else if (status == PATH_NOPERM)
		ft_dprintf(STDERR_FILENO,
				"21sh: %s: vono ne zapuskayetsia 😰\n", path);
	return (1);
}

static inline void	close_fd(t_fd *fd)
{
	if (fd->stdin != STDIN_FILENO)
		close(fd->stdin);
	if (fd->stdout != STDOUT_FILENO)
		close(fd->stdout);
	if (fd->stderr != STDERR_FILENO)
		close(fd->stderr);
}

static inline void	run_in_child(char *path, char *argv[],
						char *env[], t_fd *fd)
{
	unset_sign();
	dup2(fd->stdin, STDIN_FILENO);
	dup2(fd->stdout, STDOUT_FILENO);
	dup2(fd->stderr, STDERR_FILENO);
	close_fd(fd);
	close(fd->pipe[0]);
	close(fd->hdoc[1]);
	if (!~execve(path, argv, env))
	{
		ft_dprintf(STDERR_FILENO, "21sh: Shos' zlamalosi 😱\n");
		exit(EXIT_FAILURE);
	}
}

static inline void	run_in_parent(t_fd *fd)
{
	t_cch	*cur;

	close_fd(fd);
	close(fd->hdoc[0]);
	if (fd->hdoc_cline)
	{
		cur = fd->hdoc_cline->begin;
		while (cur)
		{
			ft_putwchar_fd(cur->ch, fd->hdoc[1]);
			cur = cur->next;
		}
	}
	close(fd->hdoc[1]);
}

int					run_ext_cmd(int argc, char *argv[], char *env[], t_fd *fd)
{
	t_path	status;
	int		pid;
	char	cmd_path[PATH_MAX];

	(void)argc;
	status = find_cmd(argv[0], get_var_value(get_env_var(env, "PATH")),
			cmd_path);
	if (status != PATH_OK)
		return (print_cmd_error(status, argv[0]));
	else
	{
		if (!(pid = fork()))
			run_in_child(cmd_path, argv, env, fd);
		else if (pid == -1)
		{
			ft_dprintf(STDERR_FILENO, "21sh: Shos' zlamalosi 😱\n");
			close_fd(fd);
			return (-1);
		}
		else
			run_in_parent(fd);
	}
	return (0);
}
