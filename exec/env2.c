/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env2.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 18:35:49 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/26 20:04:00 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

static unsigned int	count_env_vars(char *env[])
{
	unsigned int	ctr;

	ctr = 0;
	while (env[ctr])
		ctr++;
	return (ctr);
}

char				**copy_env(char *old_env[])
{
	char			**env;
	unsigned int	env_len;
	int				ctr;

	env_len = count_env_vars(old_env);
	if (!(env = (char **)malloc(sizeof(char **) * (env_len + 1))))
		return (NULL);
	ctr = 0;
	while (old_env[ctr])
	{
		if (!(env[ctr] = ft_strdup(old_env[ctr])))
		{
			free_env(env);
			return (NULL);
		}
		ctr++;
	}
	env[ctr] = NULL;
	return (env);
}

static bool			check_exist(char *env[], char *var_str)
{
	char	*delim;
	bool	exist;

	if (!(delim = ft_strchr(var_str, '=')))
		return (false);
	*delim = '\0';
	exist = !!get_env_var(env, var_str);
	*delim = '=';
	return (exist);
}

char				*push_env_var(char **env[], char *var_str)
{
	char			**new_env;
	char			*new_var_str;
	unsigned int	env_len;

	if (check_exist(*env, var_str))
		return (replace_env_var_str(*env, var_str));
	if (!ft_strchr(var_str, '='))
		return (NULL);
	new_var_str = ft_strdup(var_str);
	env_len = count_env_vars(*env);
	if (!(new_env = (char **)malloc(sizeof(char **) * (env_len + 2))))
	{
		free(new_var_str);
		return (NULL);
	}
	new_env[env_len + 1] = NULL;
	new_env[env_len] = new_var_str;
	while (env_len--)
		new_env[env_len] = (*env)[env_len];
	free(*env);
	*env = new_env;
	return (var_str);
}

void				del_env_var(char **env[], char *var)
{
	char			*var_str;
	char			**new_env;
	unsigned int	env_len;
	unsigned int	i;

	if (!(var_str = get_env_var(*env, var)))
		return ;
	env_len = count_env_vars(*env);
	if (!(new_env = (char **)malloc(sizeof(char **) * env_len)))
		return ;
	i = 1;
	new_env[--env_len] = NULL;
	while (env_len--)
	{
		if ((*env)[env_len + i] == var_str)
			i = 0;
		new_env[env_len] = (*env)[env_len + i];
	}
	free(var_str);
	free(*env);
	*env = new_env;
}
