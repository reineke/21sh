/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cline_handler.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 16:31:58 by mvladymy          #+#    #+#             */
/*   Updated: 2019/05/30 15:12:19 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "exec.h"

static t_rs			err_op_hdlr(t_cline *exec_cline)
{
	if (exec_cline->begin)
		ft_dprintf(STDERR_FILENO, "21sh: Nu i sho ce take `%llc'? 🤔\n",
				exec_cline->begin->ch);
	else
		ft_dprintf(STDERR_FILENO, "21sh: I ce vse? 🤔\n");
	return (READ_DROP);
}

static inline void	free_command(t_cline *exec_cline, t_cline *token_cline,
							t_pipe *pipe_list)
{
	free_pipe_list(pipe_list);
	free_cline(exec_cline);
	free_cline(token_cline);
}

t_rs				cline_handler(t_cline *cline)
{
	t_cline	*exec_cline;
	t_cline	*token_cline;
	t_op	op;
	t_pipe	*pipe_list;
	t_rs	rs;

	if (!(exec_cline = copy_cline(cline, 0)))
		return (READ_DROP);
	pipe_list = NULL;
	rs = READ_OK;
	token_cline = NULL;
	while ((op = get_token(&exec_cline, &token_cline)) != OP_END)
	{
		if (op == OP_ERR)
			rs = err_op_hdlr(exec_cline);
		if (op == OP_ERR)
			break ;
		create_ast(&pipe_list, token_cline, op);
		token_cline = NULL;
	}
	get_hdocs(pipe_list, &rs);
	if (rs == READ_OK)
		run_pipe_list(pipe_list);
	free_command(exec_cline, token_cline, pipe_list);
	return ((g_exit) ? READ_EXIT : rs);
}
